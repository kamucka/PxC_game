class CollectableObject extends GameObject { 
  private Collectable collectable;
  private Collectable collectable2;
  private GameObject replaceWith;
  private boolean willReplaceByAnotherGameObject;
  private GameObject addAnotherOne;
  private GameObject addEvenMore;
  private GameObject toRemove;
  private boolean willAddAditionalObject;
  private boolean willAddMoreObjects;
  private boolean isActive;
  private Collectable whileHovering;



  public CollectableObject(String identifier, int x, int y, int owidth, 
    int oheight, Collectable collectable) {
    this(identifier, x, y, owidth, oheight, collectable, null, null);
  }

  public CollectableObject(String identifier, int x, int y, int owidth, 
    int oheight, Collectable collectable, GameObject replaceWith, Collectable whileHovering) {
    super(identifier, x, y, owidth, oheight, collectable.getGameObjectImageFile());
    this.collectable = collectable;
    this.whileHovering = whileHovering;
    if (replaceWith != null) {
      this.replaceWith = replaceWith;
      this.willReplaceByAnotherGameObject = true;
    } else {
      this.willReplaceByAnotherGameObject = false;
    }
    isActive = true;
    setHoverImage(whileHovering.getGameObjectImageFile());
  }

  public CollectableObject(String identifier, int x, int y, int owidth, int oheight, Collectable collectable, 
    GameObject replaceWith,Collectable whileHovering, GameObject addAnotherOne) {
    this(identifier, x, y, owidth, oheight, collectable, replaceWith, whileHovering);
    this.addAnotherOne = addAnotherOne;
    willAddAditionalObject = true;
    isActive = true;
    setHoverImage(whileHovering.getGameObjectImageFile());
  }

  public CollectableObject(String identifier, int x, int y, int owidth, int oheight, Collectable collectable,
  GameObject replaceWith, Collectable whileHovering, GameObject addAnotherOne, GameObject toRemove) {
    this(identifier, x, y, owidth, oheight, collectable, replaceWith, whileHovering, addAnotherOne);
    this.addAnotherOne = addAnotherOne;
    //this.addEvenMore = addEvenMore;
    this.toRemove = toRemove;
    willAddAditionalObject = true;
    willAddMoreObjects = true;
    isActive = true;
    setHoverImage(whileHovering.getGameObjectImageFile());
  }

  public boolean getCIsActive() {
    return isActive;
  }

  @Override
    public void draw() {
    super.draw();
    if (mouseIsHovering){
      //println("HOVERING");
    }
  }

  @Override
    public void mouseClicked() {
    if (mouseIsHovering) {
      sceneManager.getCurrentScene().removeGameObject(this);
      isActive = false;
      if (willReplaceByAnotherGameObject) {
        sceneManager.getCurrentScene().addGameObject(replaceWith);
        if (willAddAditionalObject) {
          sceneManager.getCurrentScene().addGameObject(addAnotherOne);
          if (toRemove != null) {
            sceneManager.getCurrentScene().removeGameObject(toRemove);
          }
        }
      }
    }
  }
}

class MoveableObject extends GameObject {
  private boolean isSelected = false;
  private boolean willMove;
  private boolean notSettled;
  private MoveablePlaceholder ph;
  private String name;
  private String phName;
  private GameObject replaceWith;
  private GameObject toRemove;
  private boolean isActive;
  private boolean removeExtra = false;

  public MoveableObject(String identifier, int x, int y, int owidth, int oheight, Moveable moveable, Moveable whileMoving, 
  MoveablePlaceholder ph, GameObject replaceWith) {
    super(identifier, x, y, owidth, oheight, moveable.getGameObjectImageFile());
    this.name = identifier;
    this.ph = ph;
    this.replaceWith = replaceWith;
    notSettled = true;
    if (whileMoving != null) {
      this.willMove = true;
      setHoverImage(whileMoving.getGameObjectImageFile());
    } else {
      this.willMove = false;
    }
    isActive = true;
  }

  public MoveableObject(String identifier, int x, int y, int owidth, int oheight, Moveable moveable) {
    this(identifier, x, y, owidth, oheight, moveable, null, null, null);
    this.name = identifier;
  }
  
  public MoveableObject(String identifier, int x, int y, int owidth, int oheight, Moveable moveable, Moveable whileMoving,
  MoveablePlaceholder ph, GameObject replaceWith, GameObject toRemove){
    this(identifier, x, y, owidth, oheight, moveable, whileMoving, ph, replaceWith); 
    this.toRemove = toRemove;
    removeExtra = true;
  }

  //Checks if the object is above the placeholder
  private boolean abovePH() {
    if (ph != null) {
      if (willMove && ph.mouseAbove()) {
        return true;
      } else {
        return false;
      }
    } else return false;
  }
  
  public boolean getMIsActive(){
    return isActive; 
  }

  @Override
    public void draw() {
    super.draw();
  }

  @Override
    public void mouseClicked() {
    if (mouseIsHovering) {
      isSelected = !isSelected;
      if (abovePH()) {
        phName = ph.getPHName();
        if (name == phName) {
          println(name + " == " + phName);
          notSettled = false;
          x = ph.getX() + ph.getOwidth()/2 - owidth/2;
          y = ph.getY() + ph.getOheight()/2 - oheight/2;
          isActive = false;
          willMove = false;
          sceneManager.getCurrentScene().removeGameObject(this);
          if (replaceWith != null){
          sceneManager.getCurrentScene().addGameObject(replaceWith);
          if (removeExtra){
            sceneManager.getCurrentScene().removeGameObject(toRemove);
          } 
          }
          
        } else {
          x = getOX();
          y = getOY();
        }
      } else {
        x = getOX();
        y = getOY();
      }
    }
  }

  @Override
    public void mouseMoved() {
    super.mouseMoved();
    if (isSelected && notSettled && willMove) {
      x = mouseX - owidth/2;
      y = mouseY - oheight/2;
    }
  }
}

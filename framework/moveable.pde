class Moveable {
  private String name;
  private String gameObjectImageFile;
  
  public Moveable(String _name, String gameObjectImageFile) {
    this.name = _name;
    this.gameObjectImageFile = gameObjectImageFile;
  }
  
   public String getName() { 
    return name; 
  }
  
  public String getGameObjectImageFile() { 
    return gameObjectImageFile; 
  } 
  
  public boolean equals(Object obj) { 
    if (obj == this) { return true; } 
    if (obj == null || obj.getClass() != this.getClass()) { return false; } 
    Moveable otherMoveable = (Moveable) obj; 
    return otherMoveable.getName().equals(this.name);
  } 

  public int hashCode() { 
    final int prime = 13;
    return prime * this.name.hashCode();
  }
}

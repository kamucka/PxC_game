class GameObject {
  protected int x;
  protected int y;
  protected int owidth;
  protected int oheight;
  protected int oX;
  protected int oY;
  private String identifier;
  private String gameObjectImageFile;
  private boolean hasImage;
  private boolean hasHoverImage;
  private boolean isFinal;
  private PImage gameObjectImage;
  private PImage gameObjectImageHover;
  private PImage gameObjectFinalFile;
  protected boolean mouseIsHovering;
  
  public GameObject(String identifier, int x, int y, int owidth, int oheight) {
    this(identifier, x, y, owidth, oheight, "");
  }
  
  public GameObject(String identifier, int x, int y, int owidth, int oheight, String gameObjectImageFile) {
    this.identifier = identifier;
    this.x = x;
    this.y = y;
    oX = x;
    oY = y;
    this.owidth = owidth;
    this.oheight = oheight;
    this.hasImage = !gameObjectImageFile.equals(""); 
    this.gameObjectImageFile = gameObjectImageFile;
    if(this.hasImage) {
       this.gameObjectImage = loadImage(gameObjectImageFile);
    }
    hasHoverImage = false;
    mouseIsHovering = false;
    isFinal = false;
  }
  
  public void setHoverImage(String gameObjectImageHoverFile) {
    this.gameObjectImageHover = loadImage(gameObjectImageHoverFile);
    hasHoverImage = true;
  }
  
  public void setFinalImage(String gameObjectImageFinalFile){
    this.gameObjectFinalFile = loadImage(gameObjectImageFinalFile);
    isFinal = true;
  }
  
  public int getOX(){
    return oX; 
  }
  
  public int getOY(){
    return oY; 
  }
  
  public int getMOX(){
    return x;
  }
  
  public int getMOY(){
    return y; 
  }
  
  public int getMOW(){
    return owidth;
  }
  
  public int getMOH(){
    return oheight; 
  }
  
  public void draw() {
    if(hasImage) {
      if(mouseIsHovering && hasHoverImage) {
        image(gameObjectImageHover, x, y, owidth, oheight);
      } else if (isFinal){
        image(gameObjectFinalFile, x, y, owidth, oheight);
      }
      else {
        image(gameObjectImage, x, y, owidth, oheight);
      }
    }
  }
  
  public void mouseMoved() {
    mouseIsHovering = false;
    if(mouseX >= x && mouseX <= x + owidth &&
       mouseY >= y && mouseY <= y + oheight) {
        mouseIsHovering = true;
     }
  }
  
  public void mouseClicked() { }
  
  public String getIdentifier() {
    return this.identifier;
  }
  
  public String getgameObjectImageFile(){
    return this.gameObjectImageFile;
  }
  
  @Override 
  public boolean equals(Object obj) { 
    if (obj == this) { return true; } 
    if (obj == null || obj.getClass() != this.getClass()) { return false; } 
    GameObject otherGameObject = (GameObject) obj; 
    return otherGameObject.getIdentifier().equals(this.identifier);
  } 

  @Override 
  public int hashCode() { 
    final int prime = 11;
    return prime * this.identifier.hashCode();
  }
}

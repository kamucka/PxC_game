class Scene {
  private String sceneName;
  private PImage backgroundImage;
  private ArrayList<GameObject> gameObjects;

  private ArrayList<GameObject> recentlyAddedGameObjects;
  private ArrayList<GameObject> markedForDeathGameObjects;

  private ArrayList<PlayerOne> playerOne;

  private ArrayList<PlayerOne> recentlyAddedPlayerOneObjects;
  private ArrayList<PlayerOne> markedForDeathPlayerOneObjects;


  private ArrayList<PlayerTwo> playerTwo;

  private ArrayList<PlayerTwo> recentlyAddedPlayerTwoObjects;
  private ArrayList<PlayerTwo> markedForDeathPlayerTwoObjects;

  private ArrayList<PlayerThree> playerThree;

  private ArrayList<PlayerThree> recentlyAddedPlayerThreeObjects;
  private ArrayList<PlayerThree> markedForDeathPlayerThreeObjects;

  private ArrayList<Collision> col;
  private ArrayList<Collision> recentlyAddedCollisions;
  private ArrayList<Collision> markedForDeathCollisions;


  public Scene(String sceneName, String backgroundImageFile) {
    this.sceneName = sceneName;
    this.backgroundImage = loadImage(backgroundImageFile);
    gameObjects = new ArrayList<GameObject>();
    playerOne = new ArrayList<PlayerOne>();
    playerTwo = new ArrayList<PlayerTwo>();
    playerThree = new ArrayList<PlayerThree>();
    col = new ArrayList<Collision>();
    markedForDeathGameObjects = new ArrayList<GameObject>();
    recentlyAddedGameObjects = new ArrayList<GameObject>();
    markedForDeathPlayerOneObjects = new ArrayList<PlayerOne>();
    recentlyAddedPlayerOneObjects = new ArrayList<PlayerOne>();
    markedForDeathPlayerTwoObjects = new ArrayList<PlayerTwo>();
    recentlyAddedPlayerTwoObjects = new ArrayList<PlayerTwo>();
    markedForDeathPlayerThreeObjects = new ArrayList<PlayerThree>();
    recentlyAddedPlayerThreeObjects = new ArrayList<PlayerThree>();
    recentlyAddedCollisions = new ArrayList<Collision>();
    markedForDeathCollisions = new ArrayList<Collision>();
  }

  public void addGameObject(GameObject object) {
    recentlyAddedGameObjects.add(object);
  }

  public void removeGameObject(GameObject object) {
    markedForDeathGameObjects.add(object);
  }

  public void addPlayerOneObject(PlayerOne playerOne) {
    recentlyAddedPlayerOneObjects.add(playerOne);
  }

  public void removePlayerOneObject(PlayerOne playerOne) {
    markedForDeathPlayerOneObjects.add(playerOne);
  }

  public void addPlayerTwoObject(PlayerTwo playerTwo) {
    recentlyAddedPlayerTwoObjects.add(playerTwo);
  }

  public void removePlayerTwoObject(PlayerTwo playerTwo) {
    markedForDeathPlayerTwoObjects.add(playerTwo);
  }

  public void addPlayerThreeObject(PlayerThree playerThree) {
    recentlyAddedPlayerThreeObjects.add(playerThree);
  }

  public void removePlayerThreeObject(PlayerThree playerThree) {
    markedForDeathPlayerThreeObjects.add(playerThree);
  }

  public void addCollision(Collision col) {
    recentlyAddedCollisions.add(col);
  }

  public void removeCollision(Collision col) {
    markedForDeathCollisions.add(col);
  }

  public void updateScene() {
    if (markedForDeathGameObjects.size() > 0) {
      for (GameObject object : markedForDeathGameObjects) {
        gameObjects.remove(object);
      }
      markedForDeathGameObjects  = new ArrayList<GameObject>();
    }
    if (recentlyAddedGameObjects.size() > 0) {
      for (GameObject object : recentlyAddedGameObjects) {
        gameObjects.add(object);
      }
      recentlyAddedGameObjects  = new ArrayList<GameObject>();
    }

    if (markedForDeathPlayerOneObjects.size() > 0) {
      for (PlayerOne p : markedForDeathPlayerOneObjects) {
        playerOne.remove(p);
      }
      markedForDeathPlayerOneObjects  = new ArrayList<PlayerOne>();
    }
    if (recentlyAddedPlayerOneObjects.size() > 0) {
      for (PlayerOne p : recentlyAddedPlayerOneObjects) {
        playerOne.add(p);
      }
      recentlyAddedPlayerOneObjects  = new ArrayList<PlayerOne>();
    }

    if (markedForDeathPlayerTwoObjects.size() > 0) {
      for (PlayerTwo p : markedForDeathPlayerTwoObjects) {
        playerTwo.remove(p);
      }
      markedForDeathPlayerTwoObjects  = new ArrayList<PlayerTwo>();
    }
    if (recentlyAddedPlayerTwoObjects.size() > 0) {
      for (PlayerTwo p : recentlyAddedPlayerTwoObjects) {
        playerTwo.add(p);
      }
      recentlyAddedPlayerTwoObjects  = new ArrayList<PlayerTwo>();
    }

    if (markedForDeathPlayerThreeObjects.size() > 0) {
      for (PlayerThree p : markedForDeathPlayerThreeObjects) {
        playerThree.remove(p);
      }
      markedForDeathPlayerThreeObjects  = new ArrayList<PlayerThree>();
    }
    if (recentlyAddedPlayerThreeObjects.size() > 0) {
      for (PlayerThree p : recentlyAddedPlayerThreeObjects) {
        playerThree.add(p);
      }
      recentlyAddedPlayerThreeObjects  = new ArrayList<PlayerThree>();
    }

    if (markedForDeathCollisions.size() > 0) {
      for (Collision c : markedForDeathCollisions) {
        col.remove(c);
      }
      markedForDeathCollisions  = new ArrayList<Collision>();
    }
    if (recentlyAddedCollisions.size() > 0) {
      for (Collision c : recentlyAddedCollisions) {
        col.add(c);
      }
      recentlyAddedCollisions  = new ArrayList<Collision>();
    }
  }

  public void draw(int wwidth, int wheight) {
    imageMode(CORNER);
    image(backgroundImage, 0, 0, wwidth, wheight);
    for (GameObject object : gameObjects) {
      object.draw();
    }
    for (PlayerOne playerOne : playerOne) {
      playerOne.draw();
    }
    for (PlayerTwo playerTwo : playerTwo) {
      playerTwo.draw();
    }
    for (PlayerThree playerThree : playerThree) {
      playerThree.draw();
    }
    for (Collision col : col) {
      col.draw();
    }
  }

  public void mouseMoved() {
    for (GameObject object : gameObjects) {
      object.mouseMoved();
    }
  }

  public void mouseClicked() {
    for (GameObject object : gameObjects) {
      object.mouseClicked();
    }
  }

  public String getSceneName() {
    return this.sceneName;
  }
}

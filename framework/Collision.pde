class Collision {
  private PlayerOne player1;
  private PlayerTwo player2;
  private PlayerThree player3;
  private int x;
  private int y;
  private int w;
  private int h;
  private float px;
  private float py;
  private GameObject gameobject;
  private GameObject toReplace;
  private GameObject banner;
  private GameObject replaceWith;
  private MoveableObject moveable;
  private CollectableObject collectable;
  private boolean isMActive;
  private boolean isCActive;
  private boolean willReplace = false;
  private boolean reverseWork = false;
  private SceneManager sceneM;
  private Scene scene;

  //Player One
  Collision(PlayerOne player, GameObject gameobject, int cX, int cY, int cW, int cH) {
    this.player1 = player;
    this.gameobject = gameobject;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision(PlayerOne player, MoveableObject moveable, int cX, int cY, int cW, int cH) {
    this.player1 = player;
    this.moveable = moveable;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision (PlayerOne player, CollectableObject collectable, int cX, int cY, int cW, int cH) {
    this.player1 = player;
    this.collectable = collectable;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision (PlayerOne player, int cX, int cY, int cW, int cH, GameObject banner, MoveToSceneObject toReplace) {
    this.player1 = player;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
    this.toReplace = toReplace;
    this.banner = banner;
    willReplace = true;
  }

  //Player Two
  Collision(PlayerTwo player, GameObject gameobject, int cX, int cY, int cW, int cH) {
    this.player2 = player;
    this.gameobject = gameobject;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision(PlayerTwo player, MoveableObject moveable, int cX, int cY, int cW, int cH) {
    this.player2 = player;
    this.moveable = moveable;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision (PlayerTwo player, CollectableObject collectable, int cX, int cY, int cW, int cH) {
    this.player2 = player;
    this.collectable = collectable;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision (PlayerTwo player, int cX, int cY, int cW, int cH, GameObject banner, MoveToSceneObject toReplace) {
    this.player2 = player;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
    this.toReplace = toReplace;
    this.banner = banner;
    willReplace = true;
  }

  //Player Three
  Collision(PlayerThree player, GameObject gameobject, int cX, int cY, int cW, int cH) {
    this.player3 = player;
    this.gameobject = gameobject;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision(PlayerThree player, MoveableObject moveable, int cX, int cY, int cW, int cH) {
    this.player3 = player;
    this.moveable = moveable;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision (PlayerThree player, CollectableObject collectable, int cX, int cY, int cW, int cH) {
    this.player3 = player;
    this.collectable = collectable;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
  }

  Collision (PlayerThree player, int cX, int cY, int cW, int cH, GameObject banner, MoveToSceneObject toReplace) {
    this.player3 = player;
    this.x = cX;
    this.y = cY;
    this.w = cW;
    this.h = cH;
    this.toReplace = toReplace;
    this.banner = banner;
    willReplace = true;
  }

  public void draw() {


    if (player1 != null) {
      setP1X();
      setP1Y();
    } else if (player2 != null) {
      setP2X();
      setP2Y();
    } else if (player3 != null) {
      setP3X();
      setP3Y();
    }
    setMIsActive();
    setCIsActive();
    setPlayerSpeed();
  }

  public void setPlayerSpeed() {
    if ((px > x && px < x + w && py > y && py < y + h) && (isMActive || isCActive || willReplace)) {
      //player.setSpeed(0);
      //println("COLLISION");
      //println("PLAYER: " + px + ", " + py);
      if (willReplace) {
        println("FINISHED");
        sceneManager.getCurrentScene().addGameObject(banner);
        sceneManager.getCurrentScene().addGameObject(toReplace);
      } else {
        if (player1 != null) {
          player1.playerPos.x = player1.des[0].x;
          player1.playerPos.y = player1.des[0].y;
          player1.index = 1;
          player1.movingDir = 6;
          while (true) {
            if (snailDeath.isPlaying()) {
              break;
            } else {
              snailDeath.play();
            }
          }
        } else if (player2 != null) {
          player2.playerPos.x = player2.des[0].x;
          player2.playerPos.y = player2.des[0].y;
          player2.index = 1;
          player2.movingDir = 4;
          while (true) {
            if (snailDeath.isPlaying()) {
              break;
            } else {
              snailDeath.play();
            }
          }
        } else if (player3 != null) {
          player3.playerPos.x = player3.des[0].x;
          player3.playerPos.y = player3.des[0].y;
          player3.index = 1;
          player3.movingDir = 2;
          while (true) {
            if (snailDeath.isPlaying()) {
              break;
            } else {
              snailDeath.play();
            }
          }
        }
      }
    }
  }

  private void setMIsActive() {
    if (moveable != null) {
      this.isMActive = moveable.getMIsActive();
    }
  }

  private void setCIsActive() {
    if (collectable != null) {
      this.isCActive = collectable.getCIsActive();
    }
  }

  private void setP1X() {
    px = player1.getPlayerX();
  }

  private void setP1Y() {
    py = player1.getPlayerY();
  }

  private void setP2X() {
    px = player2.getPlayerX();
  }

  private void setP2Y() {
    py = player2.getPlayerY();
  }

  private void setP3X() {
    px = player3.getPlayerX();
  }

  private void setP3Y() {
    py = player3.getPlayerY();
  }
}

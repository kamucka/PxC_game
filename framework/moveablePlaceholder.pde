//phName has to be the same name as the moveable object that we want to interact with


class MoveablePlaceholder extends MoveableObject {
  private String phName;

  public MoveablePlaceholder(String identifier, int x, int y, int owidth, int oheight, Moveable placeholder, String phName) {
    super(identifier, x, y, owidth, oheight, placeholder);
    this.phName = phName;
  }

  @Override
    public void draw() {
    super.draw();
  }

//Gets the placeholders name set in phName that is used to compare it with object to check if it's the right one
  public String getPHName() {
    if (phName == null){
      return "No name";
    } else {
    return phName; 
    } 
  }

//Checks if the mouse is above the placeholder
  public boolean mouseAbove(){
    if (mouseIsHovering) {
      return true;
    } else {
      return false;
    }
  }
  
  public int getX(){
    return x;
  }
  public int getY(){
    return y;
  }
  public int getOheight(){
    return oheight; 
  }
  public int getOwidth(){
    return owidth; 
  }

  @Override
    public void mouseMoved() {
    super.mouseMoved();
    mouseAbove();
  }
}

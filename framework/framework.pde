import processing.sound.*;

int wwidth = 1600;
int wheight = 900;
SoundFile mainMusic;
SoundFile snailDeath;
SoundFile endSound;

final SceneManager sceneManager = new SceneManager();

void settings()
{
  size(wwidth, wheight);
}

void setup()
{
  mainMusic = new SoundFile(this, "ms.wav");
  snailDeath = new SoundFile(this, "sd.wav");
  endSound = new SoundFile(this, "won.wav");

  Scene main_menu = new Scene("main_menu", "main_menu.png");
  MoveToSceneObject start = new MoveToSceneObject("start_button", 700, 550, 200, 75, "start.png", "slide1", "start_h.png");
  MoveToSceneObject exit = new MoveToSceneObject("exit_button", 700, 675, 200, 75, "quit.png", "EXIT", "quit_h.png");
  main_menu.addGameObject(start);
  main_menu.addGameObject(exit);
  
  Scene slide1 = new Scene("slide1", "slide1.png");
  MoveToSceneObject gs1 = new MoveToSceneObject("goto_slide1", 0, 0, 1600, 900, "slide2");
  slide1.addGameObject(gs1);
  Scene slide2 = new Scene("slide2", "slide2.png");
  MoveToSceneObject gs2 = new MoveToSceneObject("goto_level1", 0, 0, 1600, 900, "level01");
  slide2.addGameObject(gs2);

  GameObject banner = new GameObject("banner", wwidth/2-300, wheight/2-100, 600, 200, "banner.png");
  Moveable ph = new Moveable("placeholder", "ph.png");
  //LEVEL 1
  Scene level01 = new Scene("level01", "level01.png");
  //PlayerOne playerOne = new PlayerOne(1287, 85, 100, 100, "snail.png");
  PlayerOne playerOne = new PlayerOne(1287, 85, 100, 100, "snail_r.png", "snail_l.png", "snail_ul.png", "snail_ur.png", "snail_dl.png", "snail_dr.png",
  "snail_diag_ur.png", "snail_diag_dr.png", "snail_diag_ul.png", "snail_diag_dl.png");

  //Laundry puzzle
  Moveable laundry = new Moveable("laundry", "laundry_I.png");
  Moveable laundry_m = new Moveable("laundry_moving", "laundry_M.png");
  GameObject laundry_B_empty = new GameObject("laundry_basket_empty", 846, 727, 80, 80, "basket_empty.png");  
  GameObject laundry_B_full = new GameObject("laundry_basket", 846, 727, 80, 80, "basket_full.png");
  GameObject laundry_not_I = new GameObject("laundry_not_interactive", 673, 285, 60, 60, "laundry.png");
  MoveablePlaceholder ph1 = new MoveablePlaceholder("ph_laundry", 849, 705, 72, 85, ph, "laundry_puzzle");
  MoveableObject mobj1 = new MoveableObject("laundry_puzzle", 750, 192, 80, 80, laundry, laundry_m, ph1, laundry_B_full);
  Collision c1 = new Collision(playerOne, mobj1, 746, 188, 50, 40);

  //Balcony puzzle
  Collectable balcony = new Collectable("balcony", "balcony.png");
  Collectable balcony_h = new Collectable("balcony_h", "stick_m.png");
  GameObject broken_balcony = new GameObject("balcony_b", 187, 402, 140, 60, "balcony_b.png");
  CollectableObject cobj1 = new CollectableObject("balcony_puzzle", 187, 402, 140, 20, balcony, broken_balcony, balcony_h);
  Collision c2 = new Collision(playerOne, cobj1, 241, 376, 38, 35);

  //Music puzzle
  Collectable radio_off = new Collectable("radio_OFF", "radio_off.png");
  Collectable radio_h = new Collectable("radio_h", "radio_off_h.png");
  GameObject dog = new GameObject("dog", 480, 733, 70, 60, "dog_barking.png");
  GameObject dog_sleep = new GameObject("dog_S", 480, 753, 70, 40, "dog_sleeping.png");
  GameObject radio_on = new GameObject("radio_ON", 1312, 552, 60, 60, "radio_on.png");
  CollectableObject cobj2 = new CollectableObject("music_puzzle", 1312, 552, 60, 60, radio_off, radio_on,radio_h, dog_sleep, dog);
  Collision c3 = new Collision(playerOne, cobj2, 599, 714, 80, 80);

  //Cloud puzzle
  Collectable cloud = new Collectable("cloud", "cloud.png");
  Collectable cloud_h = new Collectable("cloud_h", "cloud_h.png");
  GameObject cloud_r = new GameObject("cloud_rainy", 720, 10, 170, 170, "cloud_r.png");
  GameObject water = new GameObject("water", 791, 566, 11, 139, "water_flow2.png");
  CollectableObject cobj3 = new CollectableObject("cloud_puzzle", 21, 25, 180, 120, cloud, cloud_r, cloud_h, water);
  Collision c4 = new Collision(playerOne, cobj3, 791, 566, 11, 139);

  //Hydrant puzzle
  Collectable hydrant = new Collectable("hydrant", "hydrant.png");
  Collectable hydrant_h = new Collectable("hydrant_h", "hydrant_h.png");
  GameObject hydrant_w = new GameObject("hydrant_water", 1148, 332, 90, 525, "water_on.png"); 
  GameObject hydrant2 = new GameObject("hydrant2", 1159, 820, 64, 80, "hydrant.png");
  GameObject drain = new GameObject("drain", 0, 0, 1600, 900, "drain.png");
  CollectableObject cobj4 = new CollectableObject("hydrant_puzzle", 1159, 820, 64, 80, hydrant, hydrant_w, hydrant_h, hydrant2);
  Collision c5 = new Collision(playerOne, cobj4, 1159, 332, 64, 30);
  
  MoveToSceneObject m1 = new MoveToSceneObject("goto_level2", 767, 481, 100, 40, "next_level.png", "level02", "next_level_h.png");
  Collision c6 = new Collision(playerOne, 1464, 838, 80, 80, banner, m1);

  level01.addGameObject(ph1);
  level01.addGameObject(laundry_not_I);
  level01.addGameObject(laundry_B_empty);
  level01.addGameObject(mobj1);
  level01.addGameObject(cobj1);
  level01.addGameObject(dog);
  level01.addGameObject(cobj2);
  level01.addGameObject(cobj3);
  level01.addGameObject(cobj4);
  level01.addGameObject(drain);
  level01.addCollision(c1);
  level01.addCollision(c2);
  level01.addCollision(c3);
  level01.addCollision(c4);
  level01.addCollision(c5);
  level01.addCollision(c6);
  level01.addPlayerOneObject(playerOne);

  //LEVEL 2
  Scene level02 = new Scene("level02", "level02.png");
  //PlayerTwo playerTwo = new PlayerTwo(85, 450, 100, 100, "snail_r.png");
  PlayerTwo playerTwo = new PlayerTwo(85, 450, 100, 100, "snail_r.png", "snail_l.png", "snail_ul.png", "snail_ur.png", "snail_dl.png", "snail_dr.png",
  "snail_diag_ur.png", "snail_diag_dr.png", "snail_diag_ul.png", "snail_diag_dl.png");

  GameObject grass = new GameObject("grass", 0, 870, 1600, 40, "grass.png");
  
  //Swing puzzle
  GameObject swing = new GameObject("swing", 35, 445, 100, 300, "swing.png");
  GameObject swing_c = new GameObject("swing_cut", 35, 445, 100, 455, "swing_c.png");
  Moveable scissors_c = new Moveable("scissors_closed", "scissors_c.png");
  Moveable scissors_o = new Moveable("scissors_opened", "scissors_o.png");
  MoveablePlaceholder ph2 = new MoveablePlaceholder("ph_scissors", 35, 445, 100, 455, ph, "swing_puzzle");
  MoveableObject mobj2 = new MoveableObject("swing_puzzle", 875, 830, 70, 40, scissors_c, scissors_o, ph2, swing_c, swing);
  Collision c7 = new Collision(playerTwo, mobj2, 48, 736, 100, 40);

  //Rake puzzle
  Collectable rake_s = new Collectable("rake_standing", "rake_s.png");
  Collectable rake_h = new Collectable("rake_hovering", "rake_s_h.png");
  GameObject rake_l = new GameObject("rake_laying", 210, 550, 400, 450, "rake_l.png");
  CollectableObject rake = new CollectableObject("rake_puzzle", 260, 600, 100, 350, rake_s, rake_l, rake_h);
  Collision c8 = new Collision(playerTwo, rake, 250, 850, 50, 50);

  //Trashcan puzzle
  GameObject trashcan = new GameObject("trashcan", 827, 730, 100, 140, "trashcan.png");
  Moveable trashlid = new Moveable("trashcan_lid", "trashcan_lid.png");
  Moveable trashlid_M = new Moveable("trashcan_lid_moving", "trashcan_lid_m.png");
  GameObject trashlid_set = new GameObject("trashlid_set", 827, 694, 100, 40, "trashcan_lid.png");
  MoveablePlaceholder ph3 = new MoveablePlaceholder("ph_trashcan", 827, 690, 100, 140, ph, "trashcan_puzzle");
  MoveableObject mobj3 = new MoveableObject("trashcan_puzzle", 584, 835, 100, 40, trashlid, trashlid_M, ph3, trashlid_set);
  Collision c9 = new Collision(playerTwo, mobj3, 856, 708, 100, 40);

  //Brick puzzle
  Collectable brick_s = new Collectable("brick_standing", "flower_s.png");
  Collectable brick_h = new Collectable("brick_h", "flower_s_h.png");
  GameObject brick_l = new GameObject("brick_lying", 1060, 857, 100, 40, "flower_l.png");
  GameObject ghost = new GameObject("easter_egg", 1065, 818, 50, 60, "ghost.png");
  GameObject ghost_d = new GameObject("easter_egg_dead", 1065, 800, 50, 60, "ghost_d.png");
  CollectableObject brick = new CollectableObject("brick", 1108, 435, 54, 80, brick_s, brick_l, brick_h, ghost_d, ghost);
  Collision c10 = new Collision(playerTwo, brick, 1071, 857, 60, 80);

  //Lift puzzle
  Collectable lift = new Collectable("lift_base", "lift.png");
  Collectable lift_h = new Collectable("lift_h", "lift_h.png");
  GameObject lift_down = new GameObject("lift_down", 1490, 800, 100, 100, "lift.png");
  GameObject rope = new GameObject("lift_rope", 1538, -5, 10, 835, "rope.png");
  CollectableObject lift_up = new CollectableObject("lift_puzzle", 1490, 0, 100, 100, lift, lift_down, lift_h, rope);
  Collision c11 = new Collision(playerTwo, lift_up, 1490, 800, 100, 100 );

  //Plank puzzle
  Collectable plank_g = new Collectable("plank_good", "balcony.png");
  Collectable plank_h = new Collectable("balcony_h", "stick_m.png");
  GameObject plank_b = new GameObject("plank_broken", 948, 157, 200, 200, "plank_b.png");
  CollectableObject plank = new CollectableObject("plank_puzzle", 948, 157, 300, 30, plank_g, plank_b, plank_h);
  Collision c12 = new Collision(playerTwo, plank, 948, 157, 300, 30);

  MoveToSceneObject m2 = new MoveToSceneObject("goto_level3", 767, 481, 100, 40, "next_level.png", "level03","next_level_h.png");
  //GameObject next_level = new GameObject("1", 767, 481, 100, 40, "next_level.png");
  Collision c13 = new Collision(playerTwo, 1128, 486, 80, 80, banner, m2);

  level02.addGameObject(swing);
  level02.addGameObject(ph2);
  level02.addGameObject(mobj2);
  level02.addGameObject(rake);
  level02.addGameObject(trashcan);
  level02.addGameObject(ph3);
  level02.addGameObject(mobj3);
  level02.addGameObject(brick);
  level02.addGameObject(ghost);
  level02.addGameObject(lift_up);
  level02.addGameObject(plank);
  level02.addCollision(c7);
  level02.addCollision(c8);
  level02.addCollision(c9);
  level02.addCollision(c10);
  level02.addCollision(c11);
  level02.addCollision(c12);
  level02.addCollision(c13);
  level02.addPlayerTwoObject(playerTwo);
  level02.addGameObject(grass);

  //LEVEL 3
  Scene level03 = new Scene("level03", "level03.png");
  //PlayerThree playerThree = new PlayerThree(100, 555, 100, 100, "snail_r.png");
  PlayerThree playerThree = new PlayerThree(100, 555, 100, 100, "snail_r.png", "snail_l.png", "snail_ul.png", "snail_ur.png", "snail_dl.png", "snail_dr.png",
  "snail_diag_ur.png", "snail_diag_dr.png", "snail_diag_ul.png", "snail_diag_dl.png");

  //Turtle puzzle
  MoveablePlaceholder ph4 = new MoveablePlaceholder("ph_turtle", 380, 410, 200, 200, ph, "turtle_puzzle");
  GameObject turtle = new GameObject("turtle_up", 380, 410, 200, 200, "turtle.png");
  GameObject turtle_w = new GameObject("turtle_water", 380, 450, 200, 200, "turtle_water.png");
  Moveable straw_c = new Moveable("straw_cut", "straw_cut.png");
  Moveable straw_f = new Moveable("straw_full", "straw.png");
  MoveableObject straw = new MoveableObject("turtle_puzzle", 5, 493, 60, 120, straw_c, straw_f, ph4, turtle_w, turtle);
  Collision c14 = new Collision(playerThree, straw, 380, 410, 200, 200);

  //Painting puzzle 961,171
  MoveablePlaceholder ph5 = new MoveablePlaceholder("ph_painting", 500, 54, 100, 450, ph, "painting_puzzle");
  Collectable painting = new Collectable("painting_still", "painting.png");
  Collectable painting_h = new Collectable("painting_h", "painting_h.png");
  GameObject painting_t = new GameObject("painting_turned", 1020, 165, 300, 500, "painting_t.png");
  GameObject stick_s = new GameObject("stick_still", 817, 165, 500, 50, "stick.png");
  Moveable stick_i = new Moveable("stick_interactive", "stick.png");
  Moveable stick_m = new Moveable("stick_moving", "stick_m.png");
  GameObject stick_t = new GameObject("stick_turned", 525, 83, 30, 450, "stick_turned.png");
  GameObject kid = new GameObject("kid_sleeping", 954, 600, 550, 250, "kid.png");
  MoveableObject stick = new MoveableObject("painting_puzzle", 817, 401, 500, 50, stick_i, stick_m, ph5, stick_t);
  CollectableObject painting_turned = new CollectableObject("painting", 817, 165, 500, 300, painting, painting_t, painting_h);
  Collision c15 = new Collision(playerThree, stick, 410, 300, 200, 200);

  //Fan puzzle
  Collectable fan_s = new Collectable("fan_spinning", "fan_spin.png");
  Collectable fan_h = new Collectable("fan_h", "fan_spin_h.png");
  GameObject fan = new GameObject("fan_still", 525, -3, 200, 100, "fan_still.png");
  CollectableObject fan_p = new CollectableObject("fan_puzzle", 553, -3, 100, 100, fan_s, fan, fan_h);
  Collision c16 = new Collision(playerThree, fan_p, 525, 0, 100, 100);

  //Clock puzzle 1316 - 1461
  Collectable clock = new Collectable("clock", "clock2.png");
  Collectable clock_h = new Collectable("clock_h", "clock2_h.png");
  GameObject clock2 = new GameObject("clock2", 1315, 60, 200, 200, "clock2.png");
  GameObject clock_s1 = new GameObject("clock_stick_1", 1331, 160, 85, 8, "clock_stick_t.png");
  GameObject clock_s2 = new GameObject("clock_stick_2", 1415, 160, 82, 8, "clock_stick_t.png");
  GameObject clock_s3 = new GameObject("clock_stick_3", 1411, 163, 8, 80, "clock_stick.png");
  CollectableObject clock_puzzle = new CollectableObject("clock_puzzle", 1315, 60, 200, 200, clock, clock_s1, clock_h, clock_s2, clock_s3); 
  Collision c17 = new Collision(playerThree, clock_puzzle, 1418, 150, 50, 50);

  //1434, 603
  MoveToSceneObject m3 = new MoveToSceneObject("goto_level4", 767, 481, 100, 40, "next_level.png", "slide3","next_level_h.png");
  Collision c18 = new Collision(playerThree, 1434, 603, 100, 100, banner, m3);

  level03.addGameObject(turtle);
  level03.addGameObject(ph4);
  level03.addGameObject(straw);
  level03.addGameObject(ph5);
  level03.addGameObject(stick);
  level03.addGameObject(stick_s);
  level03.addGameObject(painting_turned);
  level03.addGameObject(fan_p);
  level03.addGameObject(clock2);
  level03.addGameObject(clock_puzzle);
  level03.addGameObject(clock_s1);
  level03.addGameObject(clock_s3);
  level03.addGameObject(kid);

  level03.addCollision(c14);
  level03.addCollision(c15);
  level03.addCollision(c16);
  level03.addCollision(c17);
  level03.addCollision(c18);
  level03.addPlayerThreeObject(playerThree);

  //END CUT SCENE
  Scene slide3 = new Scene("slide3", "slide3.png");
  MoveToSceneObject play_again = new MoveToSceneObject("start_button", 1100, 800, 200, 75, "play_again.png", "level01", "play_again_h.png");
  MoveToSceneObject quit = new MoveToSceneObject("exit_button", 1350, 800, 200, 75, "quit.png", "EXIT", "quit_h.png");
  slide3.addGameObject(play_again);
  slide3.addGameObject(quit);

  sceneManager.addScene(main_menu);
  sceneManager.addScene(slide1);
  sceneManager.addScene(slide2);
  sceneManager.addScene(level01);
  sceneManager.addScene(level02);
  sceneManager.addScene(level03);
  sceneManager.addScene(slide3);
}

void draw()
{
  //println("MOUSE: " + mouseX + ", " + mouseY);
  sceneManager.getCurrentScene().draw(wwidth, wheight);
  sceneManager.getCurrentScene().updateScene();
}

void mouseMoved() {
  sceneManager.getCurrentScene().mouseMoved();
}

void mouseClicked() {
  sceneManager.getCurrentScene().mouseClicked();
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == ESC) {
      exit();
    }
  }
}

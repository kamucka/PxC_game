class PlayerOne {
  boolean isFinished;
  int x;
  int y;
  int w;
  int h;
  PImage playerImage;
  PImage playerRight;
  PImage playerLeft;
  PImage playerUpL;
  PImage playerUpR;
  PImage playerDownL;
  PImage playerDownR;
  PImage playerDUR;
  PImage playerDUL;
  PImage playerDDR;
  PImage playerDDL;
  PImage playerImageFinal;
  PVector playerPos;
  PVector[] des = new PVector[22];
  public int index = 1;
  private float speed = 1f;
  public int movingDir = 6;
  private int lastDir = 6;

  public PlayerOne(int x, int y, int w, int h, String playerImage) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.playerImage = loadImage(playerImage);
    playerPos = new PVector(x, y);
    init();
  }

  public PlayerOne(int x, int y, int w, int h, String playerRight, String playerLeft, String playerUpL, String playerUpR, String playerDownL, String playerDownR, 
    String playerDUR, String playerDDR, String playerDUL, String playerDDL) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.playerRight = loadImage(playerRight);
    this.playerLeft = loadImage(playerLeft);
    this.playerUpL = loadImage(playerUpL);
    this.playerUpR = loadImage(playerUpR);
    this.playerDownL = loadImage(playerDownL);
    this.playerDownR = loadImage(playerDownR);
    this.playerDUR = loadImage(playerDUR);
    this.playerDDR = loadImage(playerDDR);
    this.playerDUL = loadImage(playerDUL);
    this.playerDDL = loadImage(playerDDL);
    playerPos = new PVector(x, y);
    init();
  }

  public void init() {
    des[0] = new PVector(1287, 85);
    des[1] = new PVector(868, 85);
    des[2] = new PVector(868, 90);
    des[3] = new PVector(578, 380);
    des[4] = new PVector(578, 400);
    des[5] = new PVector(463, 400);
    des[6] = new PVector(260, 400);
    des[7] = new PVector(260, 595);
    des[8] = new PVector(625, 595);
    des[9] = new PVector(670, 640);
    des[10] = new PVector(600, 710);
    des[11] = new PVector(670, 780);
    des[12] = new PVector(600, 850);
    des[13] = new PVector(650, 900);
    des[14] = new PVector(800, 900);
    des[15] = new PVector(800, 330);
    des[16] = new PVector(1150, 330);
    des[17] = new PVector(1220, 400);
    des[18] = new PVector(1220, 700);
    des[19] = new PVector(1220, 900);
    des[20] = new PVector(1550, 900);
    des[21] = new PVector(1550, 900);
  }


  public void draw() {

    while (true) {
      if (mainMusic.isPlaying()) {
        break;
      } else {
        mainMusic.play();
      }
    }

    while (speed == 0) {
      if (endSound.isPlaying()) {
        break;
      } else {
        endSound.play();
      }
    }


    imageMode(CENTER);
    //image(playerImage, playerPos.x, playerPos.y - h/4, w, h);

    // debug method for drawing the PVector positions on the screen
    // drawPoints();

    // determine what direction to move depending on the movingDir variable
    switch(movingDir) {
    case 0:
      playerPos.y = playerPos.y - speed;
      if (lastDir >= 5 || lastDir <= 7) {
        playerImageFinal = playerUpR;
      } else if (lastDir >= 1 || lastDir <= 3) {
        playerImageFinal = playerUpL;
      }
      lastDir = 0;
      break;
    case 1 :
      playerPos.x = playerPos.x + speed;
      playerPos.y = playerPos.y - speed;
      playerImageFinal = playerDUR;
      lastDir = 1;
      break;
    case 2:
      playerPos.x = playerPos.x + speed;
      playerImageFinal = playerRight;
      lastDir = 2;
      break;
    case 3:
      playerPos.x = playerPos.x + speed;
      playerPos.y = playerPos.y + speed;
      playerImageFinal = playerDDR;
      lastDir = 3;
      break;
    case 4:
      playerPos.y = playerPos.y + speed;
      if (lastDir >= 5 || lastDir <= 7) {
        playerImageFinal = playerDownR;
      } else if (lastDir >= 1 || lastDir <= 3) {
        playerImageFinal = playerDownL;
      }
      lastDir = 4;
      break;
    case 5:
      playerPos.x = playerPos.x - speed;
      playerPos.y = playerPos.y + speed;
      playerImageFinal = playerDDL;
      lastDir = 5;
      break;
    case 6:
      playerPos.x = playerPos.x - speed;
      playerImageFinal = playerLeft;
      lastDir = 6;
      break;
    case 7:
      playerPos.x = playerPos.x - speed;
      playerPos.y = playerPos.y - speed;
      playerImageFinal = playerDUL;
      lastDir = 7;
      break;
    case 8:
      speed = 0f;
      lastDir = 8;
    }

    image(playerImageFinal, playerPos.x, playerPos.y - h/4, w, h);

    // check if we are on the last PVector or if we are going to get an ArrayOutOfBounds exception
    if (playerPos.x == des[20].x && playerPos.y == des[20].y) {
      movingDir = 8;
      index = 19;
      playerPos.x = des[20].x;
      playerPos.y = des[20].y;
      isFinished = true;
    }

    //check when we are on top of a point
    if (playerPos.x == des[index].x && playerPos.y == des[index].y) {
      //println("check");
      movingDir = calcMovingDir();
      index++;
      //println(index);
    }

    //debug
    //println(playerPos.x, playerPos.y);
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public float getPlayerX() {
    return playerPos.x;
  }

  public float getPlayerY() {
    return playerPos.y;
  }

  // debug method
  void drawPoints() {
    fill(255, 50);
    rectMode(CENTER);
    rect(des[0].x, des[0].y, 50, 50);
    fill(255);


    for (int i = 0; i < des.length; i++) {
      rect(des[i].x, des[i].y, 50, 50);
    }

    fill(0);
    text(index, des[index].x, des[index].y);
  }

  //calculate in what direction the next point is and with that information decide which direction we want to move
  int calcMovingDir() {
    int movingDir = -1;
    if (des[index].x == des[index + 1].x) {
      if (des[index].y < des[index + 1].y) {
        movingDir = 4;
      } else if (des[index].y > des[index + 1].y) {
        movingDir = 0;
      }
    } else if (des[index].x < des[index + 1].x) {
      if (des[index].y == des[index + 1].y) {
        movingDir = 2;
      } else if (des[index].y < des[index + 1].y) {
        movingDir = 3;
      } else if (des[index].y > des[index + 1].y) {
        movingDir = 1;
      }
    } else if (des[index].x > des[index + 1].x) {
      if (des[index].y == des[index + 1].y) {
        movingDir = 6;
      } else if (des[index].y < des[index + 1].y) {
        movingDir = 5;
      } else if (des[index].y > des[index + 1].y) {
        movingDir = 7;
      }
    }
    return movingDir;
  }
}

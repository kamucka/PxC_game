class Player {
  int x;
  int y;
  int w;
  int h;
  PImage playerImage;
  PVector playerPos;
  PVector[] des = new PVector[17];
  private int index = 1;
  private float speed = 1f;
  private int movingDir = 6;

  public Player(int x, int y, int w, int h, String playerImage) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.playerImage = loadImage(playerImage);
    playerPos = new PVector(x, y);
    init();
  }
  
    public void init() {
    des[0] = new PVector(playerPos.x, playerPos.y);
    des[1] = new PVector(940, 155);
    des[2] = new PVector(940, 209); 
    des[3] = new PVector(704, 209); 
    des[4] = new PVector(463, 450); 
    des[5] = new PVector(463, 380); 
    des[6] = new PVector(155, 380); 
    des[7] = new PVector(155, 550); 
    des[8] = new PVector(465, 550); 
    des[9] = new PVector(465, 815); 
    des[10] = new PVector(650, 815); 
    des[11] = new PVector(650, 350);
    des[12] = new PVector(970, 350);
    des[13] = new PVector(1020, 400);
    des[14] = new PVector(1020, 720);
    des[15] = new PVector(1460, 720);
    des[16] = new PVector(1460, 720);
  }


  public void draw() {

    imageMode(CENTER);
    image(playerImage, playerPos.x, playerPos.y, w, h);

    // debug method for drawing the PVector positions on the screen
    // drawPoints();

    // determine what direction to move depending on the movingDir variable
    switch(movingDir) {
    case 0:
      playerPos.y = playerPos.y - speed;
      break;
    case 1 : 
      playerPos.x = playerPos.x + speed;
      playerPos.y = playerPos.y - speed;
      break;
    case 2:
      playerPos.x = playerPos.x + speed;
      break;
    case 3:
      playerPos.x = playerPos.x + speed;
      playerPos.y = playerPos.y + speed;
      break;
    case 4:
      playerPos.y = playerPos.y + speed;
      break;
    case 5:
      playerPos.x = playerPos.x - speed;
      playerPos.y = playerPos.y + speed;
      break;
    case 6:
      playerPos.x = playerPos.x - speed;
      break;
    case 7:
      playerPos.x = playerPos.x - speed;
      playerPos.y = playerPos.y - speed;
      break;
    case 8:
      speed = 0f;
    }

    // check if we are on the last PVector or if we are going to get an ArrayOutOfBounds exception
    if (playerPos.x == des[15].x && playerPos.y == des[15].y) {
      movingDir = 8;
      index = 15;
      playerPos.x = des[15].x;
      playerPos.y = des[15].y;

    }

    //check when we are on top of a point
    if (playerPos.x == des[index].x && playerPos.y == des[index].y) {
      println("check");
      movingDir = calcMovingDir();
      index++;
      println(index);
    }

    //debug
    println(playerPos.x, playerPos.y);
  }


  // debug method
  void drawPoints() {
    fill(255, 50);
    rectMode(CENTER);
    rect(des[0].x, des[0].y, 50, 50);
    fill(255);


    for (int i = 0; i < des.length; i++) {
      rect(des[i].x, des[i].y, 50, 50);
    }

    fill(0);
    text(index, des[index].x, des[index].y);
  }

  //calculate in what direction the next point is and with that information decide which direction we want to move
  int calcMovingDir() {
    int movingDir = -1;
    if (des[index].x == des[index + 1].x) {
      if (des[index].y < des[index + 1].y) {
        movingDir = 4;
      } else if (des[index].y > des[index + 1].y) {
        movingDir = 0;
      }
    } else if (des[index].x < des[index + 1].x) {
      if (des[index].y == des[index + 1].y) {
        movingDir = 2;
      } else if (des[index].y < des[index + 1].y) {
        movingDir = 3;
      } else if (des[index].y > des[index + 1].y) {
        movingDir = 1;
      }
    } else if (des[index].x > des[index + 1].x) {
      if (des[index].y == des[index + 1].y) {
        movingDir = 6;
      } else if (des[index].y < des[index + 1].y) {
        movingDir = 5;
      } else if (des[index].y > des[index + 1].y) {
        movingDir = 7;
      }
    } 
    return movingDir;
  }
}

class Scene {
  private String sceneName;
  private PImage backgroundImage;
  private ArrayList<GameObject> gameObjects;
  
  private ArrayList<GameObject> recentlyAddedGameObjects;
  private ArrayList<GameObject> markedForDeathGameObjects;
  
  private ArrayList<Player> player;
  
  private ArrayList<Player> recentlyAddedPlayerObjects;
  private ArrayList<Player> markedForDeathPlayerObjects;
  
  
  public Scene(String sceneName, String backgroundImageFile) {
    this.sceneName = sceneName;
    this.backgroundImage = loadImage(backgroundImageFile);
    gameObjects = new ArrayList<GameObject>();
    player = new ArrayList<Player>();
    markedForDeathGameObjects = new ArrayList<GameObject>();
    recentlyAddedGameObjects = new ArrayList<GameObject>();
    markedForDeathPlayerObjects = new ArrayList<Player>();
    recentlyAddedPlayerObjects = new ArrayList<Player>();
  }
  
  public void addGameObject(GameObject object) {
    recentlyAddedGameObjects.add(object);
  }
  
  public void removeGameObject(GameObject object) {
    markedForDeathGameObjects.add(object);
  }
  
  public void addPlayerObject(Player player) {
    recentlyAddedPlayerObjects.add(player);
  }
  
  public void removePlayerObject(Player player) {
    markedForDeathPlayerObjects.add(player);
  }
  
  public void updateScene() {
    if(markedForDeathGameObjects.size() > 0) {
      for(GameObject object : markedForDeathGameObjects) {
        gameObjects.remove(object);
      }
      markedForDeathGameObjects  = new ArrayList<GameObject>();
    }
    if(recentlyAddedGameObjects.size() > 0) {
      for(GameObject object : recentlyAddedGameObjects) {
        gameObjects.add(object);
      }
      recentlyAddedGameObjects  = new ArrayList<GameObject>();
    }
    
    if(markedForDeathPlayerObjects.size() > 0) {
      for(Player p : markedForDeathPlayerObjects) {
        player.remove(p);
      }
      markedForDeathPlayerObjects  = new ArrayList<Player>();
    }
    if(recentlyAddedPlayerObjects.size() > 0) {
      for(Player p : recentlyAddedPlayerObjects) {
        player.add(p);
      }
      recentlyAddedPlayerObjects  = new ArrayList<Player>();
    }
  }
  
  public void draw(int wwidth, int wheight) {
    imageMode(CORNER);
    image(backgroundImage, 0, 0, wwidth, wheight);
    for(GameObject object : gameObjects) {
      object.draw();
    }
    for(Player player : player) {
      player.draw();
    }
  }
  
  public void mouseMoved() {
    for(GameObject object : gameObjects) {
      object.mouseMoved();
    }
  }
  
  public void mouseClicked() {
    for(GameObject object : gameObjects) {
      object.mouseClicked();
    }
  }
  
  public String getSceneName() {
    return this.sceneName;
  }
}

int wwidth = 1600;
int wheight = 900;

final SceneManager sceneManager = new SceneManager();
final InventoryManager inventoryManager = new InventoryManager();

void settings()
{
  size(wwidth, wheight);
}

void setup()
{
  
  
  Scene scene01 = new Scene("scene01", "mainmenubackground.jpg");
  MoveToSceneObject object2 = new MoveToSceneObject("goToScene02_scene01", width / 2 - 100, height / 2 - 76 / 2, 200, 75, "playbutton.png", "scene02");
  scene01.addGameObject(object2);
  
  
  
  Scene scene02 = new Scene("scene02", "level_one.png");
  Player player = new Player(1327, 155, 200, 170, "snail.png");
  scene02.addPlayerObject(player);
   
  
  
  
  sceneManager.addScene(scene01);
  sceneManager.addScene(scene02);

}

void draw()
{
  sceneManager.getCurrentScene().draw(wwidth, wheight);
  sceneManager.getCurrentScene().updateScene();
  inventoryManager.clearMarkedForDeathCollectables();
  println("x: " + mouseX, "y: " + mouseY);
}

void mouseMoved() {
  sceneManager.getCurrentScene().mouseMoved();
}

void mouseClicked() {
  sceneManager.getCurrentScene().mouseClicked();
}
